CREATE INDEX ON :Action(reg_num);
CREATE CONSTRAINT ON (b:Brand) ASSERT b.value IS UNIQUE;
CREATE CONSTRAINT ON (b:Body) ASSERT b.value IS UNIQUE;
CREATE CONSTRAINT ON (c:Color) ASSERT c.value IS UNIQUE;
CREATE CONSTRAINT ON (f:Fuel) ASSERT f.value IS UNIQUE;
CREATE CONSTRAINT ON (k:Kind) ASSERT k.value IS UNIQUE;
CREATE CONSTRAINT ON (y:Year) ASSERT y.value IS UNIQUE;
CREATE CONSTRAINT ON (m:Month) ASSERT m.value IS UNIQUE;
CREATE CONSTRAINT ON (m:Model) ASSERT m.value IS UNIQUE;
CREATE CONSTRAINT ON (o:Operation) ASSERT o.code IS UNIQUE;
LOAD CSV WITH HEADERS FROM "" AS csvLine FIELDTERMINATOR ';'
MERGE (color:Color {value: csvLine.color})
MERGE (fuel:Fuel {value: csvLine.fuel})
MERGE (brand:Brand {value: trim(substring(csvLine.brand, 0, (length(csvLine.brand) - length(csvLine.model))))})
MERGE (model:Model{value: csvLine.model})
MERGE (kind:Kind {value: csvLine.kind})
MERGE (y:Year {value: split(csvLine.d_reg, '-')[0]})
MERGE (m:Month {value: split(csvLine.d_reg, '-')[1]})
MERGE (o:Operation {code: toInteger(csvLine.oper_code), name: trim(substring(csvLine.oper_name, length(csvLine.oper_code) + 3, length(csvLine.oper_name) - length(csvLine.oper_code) - 3))})
MERGE (body:Body {name: csvLine.body})
CREATE (a:Action {reg_num: csvLine.n_reg_new, operation_date: csvLine.d_reg, koatuu: csvLine.reg_add_koatuu, make_year: toInteger(csvLine.make_year), capacity: toInteger(csvLine.capacity), weight: toInteger(csvLine.total_weight)})
CREATE (y)<-[:YEAR]-(a)
CREATE (m)<-[:MONTH]-(a)
CREATE (color)<-[:COLOR]-(a)
CREATE (fuel)<-[:FUEL]-(a)
CREATE (brand)<-[:BRAND]-(a)
CREATE (model)<-[:MODEL]-(a)
CREATE (body)<-[:BODY]-(a)
CREATE (o)<-[:OPERATION]-(a)
CREATE (kind)<-[:KIND]-(a)