# -*- coding: utf-8 -*-
import argparse
import csv
import uuid
from py2neo import Graph
from py2neo.data import Node, Relationship


graph = Graph(host='localhost', password='neo')


def printProgressBar (iteration, total, prefix='', suffix='', decimals=2, length=100, fill='█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end='\r')
    # Print New Line on Complete
    if iteration == total:
        print()


def prepare_graph():
    graph.run("CREATE INDEX ON :Action(reg_num);")
    graph.run("CREATE CONSTRAINT ON (b:Brand) ASSERT b.value IS UNIQUE;")
    graph.run("CREATE CONSTRAINT ON (b:Body) ASSERT b.value IS UNIQUE;")
    graph.run("CREATE CONSTRAINT ON (c:Color) ASSERT c.value IS UNIQUE;")
    graph.run("CREATE CONSTRAINT ON (f:Fuel) ASSERT f.value IS UNIQUE;")
    graph.run("CREATE CONSTRAINT ON (k:Kind) ASSERT k.value IS UNIQUE;")
    graph.run("CREATE CONSTRAINT ON (y:Year) ASSERT y.id IS UNIQUE;")
    graph.run("CREATE CONSTRAINT ON (m:Month) ASSERT m.id IS UNIQUE;")
    graph.run("CREATE CONSTRAINT ON (m:Model) ASSERT m.value IS UNIQUE;")
    graph.run("CREATE CONSTRAINT ON (o:Operation) ASSERT o.id IS UNIQUE;")


def create_node(label, properties, primary_key='value'):
    n = Node(label, **properties)
    graph.merge(n, label, primary_key)
    return n


def create_relation(from_node, to_node, rel_type, properties={}):
    if properties:
        r = Relationship(from_node, rel_type, to_node, **properties)
    else:
        r = Relationship(from_node, rel_type, to_node)
    graph.merge(r)


def run(params):
    operations_cache = {}
    with open(params.file, 'r') as f:
        print("Calculating...")
        reader = csv.DictReader(f, delimiter=';')
        rows_count = sum(1 for row in reader)
        printProgressBar(0, rows_count, prefix='Progress:', suffix='Complete', length=50)
        i = 1
        f.seek(0)
        reader.__next__()
        for row in reader:
            operation_date = row['d_reg']
            if params.date and params.date > operation_date:
                printProgressBar(i, rows_count, prefix="Progress:", suffix="Complete", length=50)
                i += 1
                continue
            if row['oper_code'] not in operations_cache:
                operations_cache[row['oper_code']] = row['oper_name'].replace(f"{row['oper_code']} - ", '').strip()

            brand = row['brand'].replace(row['model'], '').strip()
            year, month, _ = operation_date.split('-')
            row['year'] = int(year)
            row['month'] = int(month)
            row['oper_name'] = operations_cache[row['oper_code']]
            row['brand'] = brand
            to_int = ('oper_code', 'make_year', 'capacity', 'total_weight')
            for key in to_int:
                if row[key] != 'NULL':
                    row[key] = int(row[key])
                else:
                    row[key] = 0
            graph.run(
                "MERGE (color:Color {value: {color}})"
                "MERGE (fuel:Fuel {value: {fuel}})"
                "MERGE (brand:Brand {value: {brand}})"
                "MERGE (model:Model{value: {model}})"
                "MERGE (kind:Kind {value: {kind}})"
                "MERGE (y:Year {id: {year}})"
                "MERGE (m:Month {id: {month}})"
                "MERGE (o:Operation {id: {oper_code}, name: {oper_name}})"
                "MERGE (body:Body {value: {body}})"
                "CREATE (a:Action {reg_num: {n_reg_new}, operation_date: {d_reg}, koatuu: {reg_addr_koatuu}, \
                make_year: {make_year}, capacity: {capacity}, weight: {total_weight}})"
                "CREATE (y)<-[:YEAR]-(a)"
                "CREATE (m)<-[:MONTH]-(a)"
                "CREATE (color)<-[:COLOR]-(a)"
                "CREATE (fuel)<-[:FUEL]-(a)"
                "CREATE (brand)<-[:BRAND]-(a)"
                "CREATE (model)<-[:MODEL]-(a)"
                "CREATE (body)<-[:BODY]-(a)"
                "CREATE (o)<-[:OPERATION]-(a)"
                "CREATE (kind)<-[:KIND]-(a)"
                "MERGE (model)<-[:BRANDMODEL]-(brand)", **row
            )
            i += 1
            printProgressBar(i, rows_count, prefix="Progress:", suffix="Complete", length=50)


def main():
    parser = argparse.ArgumentParser(description="CSV Importer")
    parser.add_argument('file', type=str, help='Path to csv file')
    parser.add_argument('date', type=str, help='Start date import')
    params = parser.parse_args()
    run(params)


if __name__ == "__main__":
    main()